# Sistema de gerenciamento de clientes Q2 💰

## 🚀 Começando

Projeto feito para teste na empresa Q2Bank.
Credenciais para acesso desse sistema:
email: admin@admin.com
senha: testeQ2Bank

### 📋 Pré-requisitos

Node
Yarn

### 🔧 Instalação

    Para baixar as dependências, no terminal do VsCode digitar o comando: yarn;
    Para rodar o projeto em ambiente de desenvolvimento é necessário rodar os comandos:
        yarn dev
        yarn server

### 🔧 Melhorias

    - Funcionalidade de Log out
    - Implementção de Redux
    - Usar NextAuth ou alguma outra forma de validar login e o JWT, modelo feito foi apenas simbolico
    - Criar pasta separada para configuração do axios e paras requisições em sí
    - Melhorias de layout
    - Criar variáveis de ambiente.

## 🛠️ Construído com

* [NextJs](https://nextjs.org) - O framework web usado
* [ChakraUi](https://chakra-ui.com/) - Component library
* [react-hook-form](https://react-hook-form.com) - Usado para lidar com forms
* [json-server](https://www.npmjs.com/package/json-server) - FakeAPI


## ✒️ Autores

* **Pedro Gonzales**


---