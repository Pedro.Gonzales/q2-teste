import { FormControl, FormErrorMessage, FormLabel, Input as ChakraInput, InputProps as ChakraInputProps } from "@chakra-ui/react";
import { forwardRef, ForwardRefRenderFunction } from "react";
import { FieldError } from "react-hook-form";

interface InputProps extends ChakraInputProps{
    name: string;
    label?: string;
    error?: FieldError;
    isRequired?: boolean;
}

const InputBase : ForwardRefRenderFunction<HTMLInputElement, InputProps> = ({ name, label,error, isRequired = false,  ...rest }, ref) => {
    return(
        <FormControl isInvalid={!!error} isRequired={isRequired}>
           { !!label && <FormLabel htmlFor={name}> {label} </FormLabel>}
            <ChakraInput 
              id={name}
              name={name} 
              bg='transparent'
              borderColor='whiteAlpha.500'
              focusBorderColor='green.100'
              color= 'white'
              variant='filled'
              _hover={{
                bgColor: 'blue.900'
              }}
              ref={ref}
              {...rest}
            />
            {!!error && (
              <FormErrorMessage>
                {error.message}
              </FormErrorMessage>
            )}
          </FormControl>
    )
}

export const Input = forwardRef(InputBase)