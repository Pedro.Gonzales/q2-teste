import { Avatar, Box, Flex, Icon, IconButton, Text, useBreakpointValue } from '@chakra-ui/react';
import Head from 'next/head';
import { RiMenuLine } from 'react-icons/ri';

import { useSidebarFloating } from '../../contexts/SidebarFloatingContext';

interface HeaderProps{
    title: string;
}

export function Header({title} : HeaderProps){
    const { onOpen } = useSidebarFloating();

    const isWideVersion = useBreakpointValue({
        base: false,
        lg: true
    });

    return(
       <Flex 
        as='header'
        w='100%'
        maxWidth={1480}
        h='20'
        mx='auto'
        mt='4'
        px='6'
        align='center'
        >
            { !isWideVersion && (
                <IconButton 
                icon={<Icon as={RiMenuLine}/>}
                fontSize='24'
                variant='unstyled'
                onClick={onOpen}
                aria-label='Open Navigation'
                mr='2'/>
            )}
            <Head>
                <title>{title}</title>
            </Head>
            <Text
             fontSize={['2xl', '3xl']} 
             fontWeight='bold' 
             letterSpacing='tight' 
             w='64'>
                Q2 Bank
            </Text>

            <Flex
             align='center'
             ml='auto'
            >
                {isWideVersion && (
                    <Box mr='4' textAlign='right'>
                        <Text>Pedro Gonzales</Text>
                    </Box>
                )}
                <Avatar bg='yellow.500' name='Pedro Gonzales'/>
            </Flex>
       </Flex>
    )
}