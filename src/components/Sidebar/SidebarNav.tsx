import { Icon, Stack, Text, Link as ChakraLink} from "@chakra-ui/react";
import { BsPersonFill } from 'react-icons/bs';
import { AiOutlineAreaChart } from 'react-icons/ai';
import { CgProfile } from 'react-icons/cg';
import { ActiveLink } from "../ActiveLink";

export function SidebarNav(){
    return(
        <Stack spacing='12' align='flex-start'>
            <ActiveLink href='/clientes' passHref>
                <ChakraLink display='flex' alignItems='center'>
                    <Icon as={BsPersonFill} fontSize='20'/>
                    <Text ml='4' fontWeight='bold'>Cliente</Text>
                </ChakraLink>
            </ActiveLink>
            <ActiveLink href='/dash' passHref>
                <ChakraLink display='flex' alignItems='center'>
                    <Icon as={AiOutlineAreaChart} fontSize='20'/>
                    <Text ml='4' fontWeight='bold'>DashBoard</Text>
                </ChakraLink>
            </ActiveLink>
            <ActiveLink href='/perfil' passHref>
                <ChakraLink display='flex' alignItems='center'>
                    <Icon as={CgProfile} fontSize='20'/>
                    <Text ml='4' fontWeight='bold'>Perfil</Text>
                </ChakraLink>
            </ActiveLink>
        </Stack>
    )
}