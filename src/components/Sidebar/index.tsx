import { Box, Drawer, DrawerBody, DrawerCloseButton, DrawerContent, DrawerHeader, DrawerOverlay, useBreakpointValue } from "@chakra-ui/react";

import { useSidebarFloating } from "../../contexts/SidebarFloatingContext";

import { SidebarNav } from "./SidebarNav";


export function Sidebar(){
    const { isOpen, onClose} = useSidebarFloating();

    const isFloatingSidebar = useBreakpointValue({
        base: true,
        lg: false
    });

    if(isFloatingSidebar){
        return(
            <Drawer isOpen={isOpen} placement='left' onClose={onClose} >
                <DrawerOverlay>
                    <DrawerContent bg='blue.800' p='4'>
                        <DrawerCloseButton mt='6'/>
                        <DrawerHeader> Navegação </DrawerHeader>

                        <DrawerBody>
                            <SidebarNav/>
                        </DrawerBody>
                    </DrawerContent>
                </DrawerOverlay>
            </Drawer>
        )
    }
    return(
        <Box as='aside' mr='8' borderRadius={8} border='2px' borderColor={'whiteAlpha.700'} w='15%' p='8' maxHeight='30rem'>
            <SidebarNav/>
        </Box>
    )
}