import { useDisclosure, UseDisclosureReturn } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { createContext, ReactNode, useContext, useEffect } from "react";

interface SidebarFloatingProviderProps {
    children: ReactNode;
}

type SidebarFloatingData = UseDisclosureReturn

const SidebarFloatingContext = createContext({} as SidebarFloatingData);

export function SidebarFloatingProvider({children} : SidebarFloatingProviderProps){
    const disclosure = useDisclosure();
    const router = useRouter();

    useEffect(()=>{
        disclosure.onClose()
    },[router.asPath]);
    
    return(
        <SidebarFloatingContext.Provider value={disclosure}>
            {children}
        </SidebarFloatingContext.Provider>
    )
}

export const useSidebarFloating = () => useContext(SidebarFloatingContext)