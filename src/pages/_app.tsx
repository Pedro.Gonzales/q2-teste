import type { AppProps } from 'next/app';
import { ChakraProvider} from '@chakra-ui/react';

import { SidebarFloatingProvider } from '../contexts/SidebarFloatingContext';

import { theme } from '../styles/theme';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <ChakraProvider theme={theme}>
        <SidebarFloatingProvider>
            <Component {...pageProps} />
        </SidebarFloatingProvider>
      </ChakraProvider>
    </>
  )
}

export default MyApp
