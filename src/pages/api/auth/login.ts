import { NextApiRequest, NextApiResponse } from "next";

import { sign } from 'jsonwebtoken';
import { serialize } from 'cookie';

const secret = 'bestSecretKey69'

export default async function (req: NextApiRequest, res: NextApiResponse){
    const { email, password } = req.body;

    if(email === 'admin@admin.com' && password === 'testeQ2Bank'){
        const token = sign({
            exp: Math.floor(Date.now() / 1000) + 60 * 60 *24, // 1 dia
            username: 'Admin'
        }, secret)

        const serialized = serialize('Q2JWT', token, {
            httpOnly: true,
            sameSite: 'strict',
            maxAge: 60*60*24,
            path: '/'
        })

        res.setHeader('Set-Cookie', serialized);
        res.status(200).json({message: 'Success'});
    }else{
        res.status(400).json({message: 'Invalid credentials.'});
    }
}