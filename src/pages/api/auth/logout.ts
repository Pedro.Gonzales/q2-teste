import { NextApiRequest, NextApiResponse } from "next";

import { serialize } from "cookie";

export default async function (req: NextApiRequest, res: NextApiResponse){
    const { cookies } = req;

    const jwt = cookies.Q2JWT;

    if(!jwt){
        res.status(400).json({message: 'No one is logged in'})
    }else{
        const serialized = serialize('Q2JWT', '', {
            httpOnly: true,
            sameSite: 'strict',
            maxAge: -1,
            path: '/'
        })

        res.setHeader('Set-Cookie', serialized);
        res.status(200).json({message: 'Successfuly logged out'});
    }
}