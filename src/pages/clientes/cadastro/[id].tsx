import { Box, Button, Divider, Flex, Heading, SimpleGrid, Stack, useBreakpointValue, useToast } from "@chakra-ui/react";
import axios from "axios";
import { GetServerSideProps } from "next";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { ClientData } from "..";

import { Input } from "../../../components/Form/Input";
import { Header } from "../../../components/Header";
import { Sidebar } from "../../../components/Sidebar";

type EditClientData = {
    nome: string;
    documento: string;
    banco: string;
    codigo: string;
    agencia: string;
    conta: string;
}

export default function Cadastro(){
    const {query,isReady} = useRouter();
    const toast = useToast()

    useEffect(()=>{
        if(isReady){
            //@ts-ignore
            getClientById(query.id)
        }
    },[isReady])

    const { handleSubmit, register, formState, formState:{errors}, setValue} = useForm();

    async function getClientById(id: string){
        const response = await axios.get(`https://q2-teste.herokuapp.com/clients?id=${id}`);
        const data : ClientData = response.data[0]

        setValue('nome', data.name);
        setValue('documento', data.document);
        setValue('banco', data.bank.bankName);
        setValue('agencia', data.bank.agency);
        setValue('conta', data.bank.account);
        setValue('codigo', data.bank.code);
    }

    const handleCreateUser: SubmitHandler<EditClientData> = async (values) => {
        const {status} = await axios.put(`https://q2-teste.herokuapp.com/clients/${query.id}`,{
            name: values.nome,
            document: values.documento,
            bank: {
            bankName: values.banco,
            code: values.codigo,
            agency: values.agencia,
            account: values.conta
    }
        });
        if(status === 200){
            toast({
                description: "O cliente foi atualizado com sucesso.",
                status: 'success',
                duration: 9000,
                isClosable: false,
                position: 'top'
              })
        }
    }
    return(
        <Box h='auto'>
            <Header title='Edição Clientes'/>

            <Flex w={['auto', '100%']} my='6' maxWidth={1480} mx='auto' px='6' h='70%'>
                <Sidebar/>
                {/* @ts-ignore */}
                <Box as='form' onSubmit={handleSubmit(handleCreateUser)} flex='1' borderRadius={8} bg='blue.700' p={['6', '8']}>
                    <Heading size='lg' fontWeight='normal'> Editar Cliente </Heading>

                    <Divider my='6' borderColor='gray.700'/>

                    <Stack spacing='6'>
                        <SimpleGrid minChildWidth='240px' spacing={['6', '8']} w='100%'>
                            <Input 
                                isRequired
                                label="Nome"
                                error={errors.nome}
                                {...register('nome',{ 
                                    required: 'Nome é obrigatório',
                                })}/>    
                            <Input 
                                isRequired
                                label="Documento"
                                error={errors.documento}
                                {...register('documento',{ 
                                    required: 'Documento é obrigatório'
                                })}/>    
                        </SimpleGrid>    
                        <SimpleGrid minChildWidth='240px' spacing={['6', '8']} w='100%'>
                            <Input 
                            isRequired
                            label="Banco"
                            error={errors.banco}
                            {...register('banco',{ 
                                required: 'Nome do banco é obrigatório'
                            })}/>    
                            <Input 
                            isRequired
                            label="Código"
                            error={errors.codigo}
                            {...register('codigo',{ 
                                required: 'Código é obrigatório'
                            })}/>    
                        </SimpleGrid>    
                        <SimpleGrid minChildWidth='240px' spacing={['6', '8']} w='100%'>    
                            <Input 
                            isRequired
                            label="Agência"
                            error={errors.agencia}
                            {...register('agencia',{ 
                                required: 'Agência é obrigatório'
                            })}/>  
                            <Input 
                            isRequired
                                label="Conta"
                                error={errors.conta}
                                {...register('conta',{ 
                                    required: 'Conta é obrigatório'
                                })}/>  
                        </SimpleGrid>               
                    </Stack>

                    <Flex mt='10' justify='flex-end'>
                        <Stack spacing='4' direction='row'>
                            <Link href='/clientes' passHref>
                                <Button 
                                as='a' 
                                colorScheme='whiteAlpha'
                                > 
                                    Cancelar 
                                </Button>
                            </Link>
                            <Button 
                                colorScheme='yellow'
                                type="submit"
                                isLoading={formState.isSubmitting}>
                                 Confirmar 
                            </Button>
                        </Stack>
                    </Flex> 
                </Box>
            </Flex>
        </Box>
    )
}

export const getServerSideProps : GetServerSideProps = async ({req, res}) => {
    const isAuthenticated = (headers: any) => {
      if (!headers?.cookie) {
        return undefined;
      }else{
        const match = headers.cookie
        .split(';')
        .find((item: any) => item.trim().startsWith('Q2JWT'));
  
        if (!match) {
            return undefined;
        }
    
        return match.split('=')[1]; 
      }
    };
  
    if (!isAuthenticated(req.headers)) {
      res.writeHead(303, { Location: '/' });
      res.end();
    }
  
    return { props: {} };
  }