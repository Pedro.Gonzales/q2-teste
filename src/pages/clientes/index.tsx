import { RiPencilLine } from 'react-icons/ri'
import Link from 'next/link';
import { useEffect, useState } from 'react';
import {  Button, Flex, Heading, Icon, IconButton, Table, Tbody, Td, Th, Thead, Tr, useBreakpointValue } from "@chakra-ui/react";

import { Header } from "../../components/Header";
import { Sidebar } from "../../components/Sidebar";
import { Pagination } from "../../components/Pagination";
import axios from 'axios';
import { GetServerSideProps } from 'next';

export type ClientData = {
    id: number,
    name: string,
    document: string,
    bank: BankData
}

type BankData = {
    bankName: string,
    code: string,
    agency: string,
    account: string
}

export default function ClientList(){
    const [currentPage, setCurrentPage] = useState(1)
    const [registers, setRegisters] = useState(0)
    const [clients, setClients] = useState<ClientData[]>([])

    const isWideVersion = useBreakpointValue({
        base: false,
        lg: true,
    })

    useEffect(()=>{
        getClients()
    },[])

    useEffect(()=>{
        getClients()
    },[currentPage])

    async function getClients(){
        const {data, headers} = await axios.get(`https://q2-teste.herokuapp.com/clients?_page=${currentPage}&_limit=10`);
        setClients(data);
        setRegisters(Number(headers['x-total-count']))
    }
    return(
        <>
        <Flex direction='column' h='auto'>
            <Header title='Pesquisa | Clientes'/>
            <Flex w='100%' my='6' maxWidth={1480} mx='auto' px='6' h='70%'>
                <Sidebar />

                <Flex direction='column' flex='1' borderRadius={8} bg='blue.700' p='8'>
                    <Flex mb='8'>
                        <Heading size='lg' fontWeight='normal'> Clientes </Heading>
                    </Flex>

                    <Table colorScheme='whiteAlpha' mb='auto'>
                        <Thead>
                            <Tr>
                                <Th color='whiteAlpha.600'> Nome </Th>
                                {isWideVersion &&<Th color='whiteAlpha.600'> Documento </Th>}
                                {isWideVersion && <Th color='whiteAlpha.600'> Banco </Th>}
                                <Th color='whiteAlpha.600' w='8'> Ações </Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                            {clients.map((cliente, index) => (
                                <Tr key={index}>
                                <Td> {cliente.name} </Td>
                                {isWideVersion && <Td>  {cliente.document} </Td>}
                                {isWideVersion && <Td> {cliente.bank.bankName} </Td>}
                                {isWideVersion ?
                                    <Td> 
                                        <Link href={`/clientes/cadastro/${cliente.id}`} passHref>
                                            <Button as='a' variant='unstyled' leftIcon={<Icon as={RiPencilLine} fontSize='16'/>} color='yellow.400'> Editar </Button>
                                        </Link>
                                    </Td>
                                    :
                                    <Td>  
                                        <Link href={`/clientes/cadastro/${cliente.id}`} passHref>
                                            <IconButton as='a' icon={<Icon as={RiPencilLine}/>} fontSize='20' variant='unstyled' aria-label='Open Navigation' color='yellow.400'/>
                                        </Link>
                                    </Td>
                                }
                            </Tr>
                            ))}
                        </Tbody>
                    </Table>
                    <Pagination totalCountOfRegisters={registers} currentPage={currentPage} onPageChange={setCurrentPage}/>
                </Flex>
            </Flex>
        </Flex>
        </>
    )
}
export const getServerSideProps : GetServerSideProps = async ({req, res}) => {
    const isAuthenticated = (headers: any) => {
      if (!headers?.cookie) {
        return undefined;
      }else{
        const match = headers.cookie
        .split(';')
        .find((item: any) => item.trim().startsWith('Q2JWT'));
  
        if (!match) {
            return undefined;
        }
    
        return match.split('=')[1]; 
      }
    };
  
    if (!isAuthenticated(req.headers)) {
      res.writeHead(303, { Location: '/' });
      res.end();
    }
  
    return { props: {} };
  }