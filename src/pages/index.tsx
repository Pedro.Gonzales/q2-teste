import { Button, Flex, Stack, useToast} from '@chakra-ui/react';

import { SubmitHandler, useForm } from 'react-hook-form';
import axios from 'axios';

import { Input } from '../components/Form/Input';
import { useRouter } from 'next/router';

type SignInFormData = {
  email: string;
  password: string;
}

const Home = () => {
    const { handleSubmit, register, formState, formState:{errors}} = useForm()
    const toast = useToast();
    const router = useRouter();

    const handleSignIn:  SubmitHandler<SignInFormData> = async ({email, password}) =>{
      await axios.post('/api/auth/login', {email, password})
      .catch(e=> 
          toast({
            description: 'Credenciais Invalidas',
            status: 'warning',
            duration: 4000,
            isClosable: false,
          }) 
        )
      .then(e => router.push('/clientes'));
    }
  return (
   <Flex 
    w='100vw' 
    h='100vh' 
    align='center' 
    justify='center'
  >
      <Flex
        as='form'
        width='100%'
        maxWidth={360}
        bg='gray.700'
        p='8'
        borderRadius={8}
        flexDir='column'
        //@ts-ignore
        onSubmit={handleSubmit(handleSignIn)}
        
      >

        <Stack spacing='4'>

         <Input   
          type='email' 
          label='E-mail' 
          error={errors.email}
          {...register('email',{ 
            required: 'E-mail obrigatório'
          })}
          /> 

         <Input
         type='password'  
         label='Senha' 
         error={errors.senha}
          {...register('password',{ 
            required: 'Senha obrigatória'
          })}
         /> 
         
        </Stack>
        <Button 
          type='submit' 
          mt='6' 
          colorScheme='blue' 
          size='lg' 
          isLoading={formState.isSubmitting}
        >
           Entrar 
          </Button>

      </Flex>
   </Flex>
  )
}

export default Home
