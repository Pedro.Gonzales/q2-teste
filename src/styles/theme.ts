import { extendTheme } from '@chakra-ui/react'

export const theme = extendTheme({
    colors:{
        blue:{
          "100": "#3795ec",  
          "300": "#256DB1",  
          "500": "#224EF2",  
          "700": "#18558d",  
        },
        grey:{
          "300": "#a8a8b3",  
        },
        cyan:{
          "500": "#00D1FF",  
        },
        yellow:{
          "100": "#EBF323",  
        },
        red:{
          "200": "#e52e4d",  
        },
        green:{
          "200": "#33cc95",  
        },
    },
    fonts:{
        heading: 'Roboto',
        body: 'Roboto'
    },
    styles:{
        global:{
            body:{
                bg: 'blue.300',
                color: 'white'
            }
        }
    }
})